package com.project.cpsc534l.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is used to extract JSON strings from a URL. The
 * URL must point to JSON content
 * @author focarizajr
 *
 */
public class JsonUrlExtractor {
	private String url;
	private JSONObject jsonObj;
	
	/**
	 * Constructor
	 * @param url
	 * 		The URL
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public JsonUrlExtractor(String _url) throws JSONException, IOException {
		url = _url;
		jsonObj = readJsonFromUrl(url);
	}
	
	/**
	 * Reads the contents of a file using a reader
	 * @param rd
	 * 		The reader object
	 * @return
	 * 		The string content of the file
	 * @throws IOException
	 */
	private String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}
	
	/**
	 * Creates a JSONObject from the JSON file contained in the webpage
	 * pointed to by the URL
	 * @param url
	 * 		The URL
	 * @return
	 * 		The JSONObject representing the JSON file
	 * @throws IOException
	 * @throws JSONException
	 */
	private JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			  BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			  String jsonText = readAll(rd);
			  //System.out.println("Length of JSON text: " + jsonText.length());
			  int firstBrace = jsonText.indexOf("{");
			  int lastBrace = jsonText.lastIndexOf("}");
			  jsonText = StringEscapeUtils.unescapeHtml4(jsonText.substring(firstBrace, lastBrace+1));
			  //System.out.println("First few characters of JSON text: '" + jsonText.substring(0,400));
			  JSONObject json = new JSONObject(jsonText);
			  return json;
		} finally {
			  is.close();
		}
	}
	
	/**
	 * Retrieves the JSON object
	 * @return
	 * 		The JSON object
	 */
	public JSONObject getJsonObj() {
		return jsonObj;
	}
	
	/**
	 * Retrieves the URL
	 * @return
	 * 		The URL
	 */
	public String getUrl() {
		return url;
	}
}