package com.project.cpsc534l.team;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import com.project.cpsc534l.skills.GradedSkill;
import com.project.cpsc534l.skills.extractor.BugReport;
import com.project.cpsc534l.skills.extractor.Developer;

public class TeamFinder {
	private String pathToTempFolder;
	private int maxNumBugReports;
	private SimpleWeightedGraph<Developer, DefaultWeightedEdge> socialGraph;
	private Map<String, Developer> developers;
	private List<BugReport> bugReports;
	
	private int startIndex; //Index of first bug report, after the train set, to consider (index 0 is maxNumBugReports, when translated to index of *all* bug reports)
	private int endIndex; //Index of last bug report, after the train set, to consider
	
	private String finalOutput; //the string that will be output to the file
	
	/**
	 * Constructor
	 * @param _maxNumBugReports
	 * 		The number of bug reports in the training set
	 * @param _socialGraph
	 * 		The social graph
	 * @param _developers
	 * 		The set of developers; Each developer must already have
	 * 		been assigned skills
	 * @param _bugReports
	 * 		The set of bug reports; each bug report in the test set
	 * 		must already have been assigned skills
	 */
	public TeamFinder(String _pathToTempFolder, int _maxNumBugReports, SimpleWeightedGraph<Developer, DefaultWeightedEdge> _socialGraph, Map<String, Developer> _developers, List<BugReport> _bugReports, int _startIndex, int _endIndex) {
		pathToTempFolder = _pathToTempFolder;
		maxNumBugReports = _maxNumBugReports;
		socialGraph = _socialGraph;
		developers = _developers;
		bugReports = _bugReports;
		
		startIndex = _startIndex;
		endIndex = _endIndex;
		if (endIndex < 0) {
			endIndex = bugReports.size() - maxNumBugReports;
		}
		
		finalOutput = "";
	}
	
	public void findTeamForAllBugReports() {
		for (int i = maxNumBugReports + startIndex; i < maxNumBugReports + endIndex; i++) {
			try {
				findTeamForBugReport(i);
			}
			catch (Exception e) {
				System.out.println("Ignoring bug report due to exception");
			}
		}
		
		createFile("teams.txt", finalOutput);
	}
	
	/**
	 * Finds the team for the bug report that appears in the
	 * specified index, in the list of bug reports
	 * @param index
	 * 		The index of the bug report in the bugReports object
	 */
	public void findTeamForBugReport(int index) {
		BugReport br = bugReports.get(index);
		System.out.println("Finding team for BR#" + index + " (id: " + br.getId() + ")");
		Map<String, GradedSkill> task = getTask(br);
		
		//Find the support set for each skill in the task
		System.out.println("Finding the support set for each skill in the task...");
		Map<String, Map<String, Developer>> supportSets = new HashMap<String, Map<String, Developer>>(); //maps the skill name to its support set
		Set<String> skillNames = task.keySet();
		for (String skillName : skillNames) {
			GradedSkill skill = task.get(skillName);
			Map<String, Developer> supportSet = supportSet(skill);
			if (supportSet.isEmpty()) {
				System.out.println("Support set is empty for skill " + skillName + " in bug report " + br.getId());
				return;
			}
			if (developersGrade(supportSet, skillName) < skill.getNormalizedGrade()) {
				System.out.println("Total grade of support set for skill " + skillName + " in bug report " + br.getId() + " is not sufficient");
				return;
			}
			
			supportSets.put(skillName, supportSet);
		}
		
		//Find the least supported skill
		System.out.println("Finding the least supported skill...");
		String leastSkillName = leastSupportedSkill(supportSets);
		Map<String, Developer> leastSkillSupportSet = supportSets.get(leastSkillName);
		
		//Find the developers within leastSupportedSkill that are closest to each developer in that same set
		System.out.println("Finding the developers supporting the least supported skill that are closest to each developer in that same set...");
		Map<String, Map<String, Developer>> closestToDeveloper = new HashMap<String, Map<String, Developer>>(); //each Map<String, Developer> represents the set of developers in leastSkillSupportSet closest to a particular developer in the same support set (same as Omega_i in algorithm of Homework 1)
		Set<String> leastSkillDeveloperEmails = leastSkillSupportSet.keySet();
		for (String leastSkillDeveloperEmail : leastSkillDeveloperEmails) {
			Developer developer = leastSkillSupportSet.get(leastSkillDeveloperEmail);
			Map<String, Developer> closestDevelopers = new HashMap<String, Developer>();
			Map<String, Developer> developersCopy = new HashMap<String, Developer>();
			for (String email : leastSkillDeveloperEmails) {
				//Make a copy of the support set for this iteration
				developersCopy.put(email, leastSkillSupportSet.get(email));
			}
			
			closestDevelopers.put(leastSkillDeveloperEmail, developer);
			developersCopy.remove(leastSkillDeveloperEmail);
			
			while (!coversSkill(closestDevelopers, task.get(leastSkillName))) {
				String closestDeveloperName = findClosestDeveloper(developersCopy, developer);
				closestDevelopers.put(closestDeveloperName, developersCopy.get(closestDeveloperName));
				developersCopy.remove(closestDeveloperName);
			}
			
			closestToDeveloper.put(leastSkillDeveloperEmail, closestDevelopers);
		}
		
		//Find all the longest shortest path distance from the nodes in a
		//set found in the previous step to the set of nodes, satisfying some
		//other skill s, that are closest to them
		Map<String, Map<String, Map<String, Double>>> shortestPathDistances = new HashMap<String, Map<String, Map<String, Double>>>(); //a collection of shortest path distances (analogous to R_isj in Homework 1 answer)
		Map<String, Map<String, Map<String, Developer>>> closestFromOtherSkills = new HashMap<String, Map<String, Map<String, Developer>>>(); //each Map<String, Developer> represents a set of developers j with the lowest R_isj values (analogous to Phi_is in Homework 1 answer)
		Map<String, Map<String, Double>> maxShortestPathFromOtherSkills = new HashMap<String, Map<String, Double>>(); //analogous to R_is in Homework 1 answer
		Map<String, Double> maxShortestPath = new HashMap<String, Double>(); //analogous to R_i in Homework 1 answer
		System.out.println("Finding the longest shortest path...");
		for (String leastSkillDeveloperEmail : leastSkillDeveloperEmails) {
			//System.out.println("For loop: i");
			shortestPathDistances.put(leastSkillDeveloperEmail, new HashMap<String, Map<String, Double>>());
			closestFromOtherSkills.put(leastSkillDeveloperEmail, new HashMap<String, Map<String, Developer>>());
			maxShortestPathFromOtherSkills.put(leastSkillDeveloperEmail, new HashMap<String, Double>());
			for (String skillName : skillNames) {
				//System.out.println("For loop: s");
				if (skillName.equals(leastSkillName)) {
					continue;
				}
				
				shortestPathDistances.get(leastSkillDeveloperEmail).put(skillName, new HashMap<String, Double>());
				closestFromOtherSkills.get(leastSkillDeveloperEmail).put(skillName, new HashMap<String, Developer>());
				
				Map<String, Developer> supportSet = supportSets.get(skillName);
				Set<String> developerEmails = supportSet.keySet();
				for (String developerEmail : developerEmails) {
					//System.out.println("For loop: j");
					Map<String, Developer> closestDevelopers = closestToDeveloper.get(leastSkillDeveloperEmail);
					//Find the longest distance among shortest path distances with developers in closestDevelopers
					double distance = findLongestShortestPathDistance(developers.get(developerEmail), closestDevelopers);
					shortestPathDistances.get(leastSkillDeveloperEmail).get(skillName).put(developerEmail, distance);
				}
				
				Map<String, Developer> closestDevelopers = new HashMap<String, Developer>();
				Map<String, Developer> developersCopy = new HashMap<String, Developer>();
				for (String email : developerEmails) {
					//Make a copy of the support set for this iteration
					developersCopy.put(email, supportSet.get(email));
				}
				Map<String, Double> shortestPathDistancesCopy = new HashMap<String, Double>();
				Set<String> shortestPathDistanceEmails = shortestPathDistances.get(leastSkillDeveloperEmail).get(skillName).keySet();
				for (String email : shortestPathDistanceEmails) {
					//Make a copy of the R_isj values
					shortestPathDistancesCopy.put(email, shortestPathDistances.get(leastSkillDeveloperEmail).get(skillName).get(email));
				}
				Map<String, Double> chosenDevelopersDistances = new HashMap<String, Double>();
				while (!coversSkill(closestDevelopers, task.get(skillName))) {
					//System.out.println("While loop");
					String closestDeveloperName = developerWithSmallestDistance(shortestPathDistancesCopy);
					closestDevelopers.put(closestDeveloperName, developersCopy.get(closestDeveloperName));
					chosenDevelopersDistances.put(closestDeveloperName, shortestPathDistancesCopy.get(closestDeveloperName));
					developersCopy.remove(closestDeveloperName);
					shortestPathDistancesCopy.remove(closestDeveloperName);
				}
				//Update Phi_is
				closestFromOtherSkills.get(leastSkillDeveloperEmail).put(skillName, closestDevelopers);
				//Update R_is
				double maxDistance = chosenDevelopersDistances.get(developerWithGreatestDistance(chosenDevelopersDistances));
				maxShortestPathFromOtherSkills.get(leastSkillDeveloperEmail).put(skillName, maxDistance);
			}
			
			//Update R_i
			Map<String, Double> distancesGathered = maxShortestPathFromOtherSkills.get(leastSkillDeveloperEmail);
			double maxDistance = distancesGathered.get(developerWithGreatestDistance(distancesGathered));
			maxShortestPath.put(leastSkillDeveloperEmail, maxDistance);
		}
		
		//Now, find the developer whose R_i value is smallest (i.e., i* in the Homework 1 answer)
		String starEmail = developerWithSmallestDistance(maxShortestPath);
		
		//Finally, set up the list of developer e-mails
		System.out.println("Adding developers from Omega_i*...");
		Map<String, Developer> theTeam = new HashMap<String, Developer>();
		//First, add the ones Omega_i* (i.e., closest with same skill)
		Set<String> teamEmails = closestToDeveloper.get(starEmail).keySet();
		for (String email : teamEmails) {
			Developer dev = developers.get(email);
			theTeam.put(email, dev);
			br.addDeveloper(email, dev);
		}
		//Next, find all devs in Phi_i*s, where s is not the same as the leastSkill
		System.out.println("Adding all other developers in the team...");
		Set<String> otherSkills = closestFromOtherSkills.get(starEmail).keySet();
		for (String otherSkill : otherSkills) {
			Map<String, Developer> otherDevelopers = closestFromOtherSkills.get(starEmail).get(otherSkill);
			Set<String> otherEmails = otherDevelopers.keySet();
			for (String otherEmail : otherEmails) {
				Developer dev = developers.get(otherEmail);
				theTeam.put(otherEmail, dev);
				br.addDeveloper(otherEmail, dev);
			}
		}
		
		System.out.println("Setting up output...");
		String output = br.getId() + "";
		Set<String> allEmailsInTeam = theTeam.keySet();
		for (String email : allEmailsInTeam) {
			output += " " + email;
		}
		System.out.println(output);
		if (finalOutput.equals("")) {
			finalOutput = output;
		}
		else {
			finalOutput += "\n" + output;
		}
	}
	
	/**
	 * Retrieves a map of the nonzero skills required by a bug report
	 * @param br
	 * 		The bug report
	 * @return
	 * 		The task (i.e., set of non-zero skills)
	 */
	private Map<String, GradedSkill> getTask(BugReport br) {
		Map<String, GradedSkill> task = new HashMap<String, GradedSkill>();
		Map<String, GradedSkill> allSkills = br.getSkills();
		Set<String> skillNames = allSkills.keySet();
		for (String skillName : skillNames) {
			GradedSkill skill = allSkills.get(skillName);
			if (skill.getNormalizedGrade() > 0.0) {
				task.put(skillName, skill);
			}
		}
		
		return task;
	}
	
	/**
	 * Find the support set of skill s, which is defined as the set
	 * of developers that have a non-zero grade for s
	 * @param s
	 * 		The skill
	 * @return
	 * 		The set of developers supporting s
	 */
	private Map<String, Developer> supportSet(GradedSkill s) {
		Map<String, Developer> supportingDevelopers = new HashMap<String, Developer>();
		Set<String> developerEmails = developers.keySet();
		for (String developerEmail : developerEmails) {
			Developer developer = developers.get(developerEmail);
			if (developer.getSkills().containsKey(s.getIdentifier()) && developer.getSkills().get(s.getIdentifier()).getNormalizedGrade() > 0.0) {
				supportingDevelopers.put(developerEmail, developer);
			}
		}
		
		return supportingDevelopers;
	}
	
	/**
	 * Determines the total normalized grade of all the developers in
	 * a set.
	 * @param developerSet
	 * 		A set of developers
	 * @param skillName
	 * 		The name of the corresponding skill
	 * @return
	 * 		The total normalized grade
	 */
	private double developersGrade(Map<String, Developer> developerSet, String skillName) {
		double total = 0;
		Set<String> developerEmails = developerSet.keySet();
		for (String developerEmail : developerEmails) {
			Developer developer = developerSet.get(developerEmail);
			total += developer.getSkills().get(skillName).getNormalizedGrade();
		}
		
		return total;
	}
	
	/**
	 * Determines if the total normalized grade of all the developers
	 * in a set of developers sufficiently covers the required grade for the skill
	 * @param supportSet
	 * 		The support set for the skill, or any set of developers
	 * @param skill
	 * 		The graded skill
	 * @return
	 */
	private boolean coversSkill(Map<String, Developer> developerSet, GradedSkill skill) {
		double requiredGrade = skill.getNormalizedGrade();
		return (developersGrade(developerSet, skill.getIdentifier()) >= requiredGrade);
	}
	
	/**
	 * Finds the least supported skill, among the skills represented by a support set
	 * @param supportSets
	 * 		The list of support sets
	 * @return
	 * 		The name of the least supported skill
	 */
	private String leastSupportedSkill(Map<String, Map<String, Developer>> supportSets) {
		int smallestSize = -1;
		String leastSkill = null;
		Set<String> skillNames = supportSets.keySet();
		for (String skillName : skillNames) {
			Map<String, Developer> supportSet = supportSets.get(skillName);
			if (smallestSize < 0 || supportSet.size() < smallestSize) {
				smallestSize = supportSet.size();
				leastSkill = skillName;
			}
		}
		
		return leastSkill;
	}
	
	/**
	 * Finds the developer in some developer set with the shortest 
	 * shortest path distance with the source developer
	 * @param developerSet
	 * 		The developer set
	 * @param sourceDeveloper
	 * 		The source developer
	 * @return
	 * 		The e-mail of the developer with the shortest shortest
	 * 		path distance with sourceDeveloper
	 */
	private String findClosestDeveloper(Map<String, Developer> developerSet, Developer sourceDeveloper) {
		String closestDeveloper = null;
		double shortestPathLength = -1;
		Set<String> developerEmails = developerSet.keySet();
		for (String developerEmail : developerEmails) {
			Developer targetDeveloper = developerSet.get(developerEmail);
			DijkstraShortestPath<Developer, DefaultWeightedEdge> shortestPathObject = new DijkstraShortestPath<Developer, DefaultWeightedEdge>(socialGraph, sourceDeveloper, targetDeveloper);
			double pathLength = shortestPathObject.getPathLength();
			if (shortestPathLength < 0 || pathLength < shortestPathLength) {
				shortestPathLength = pathLength;
				closestDeveloper = targetDeveloper.getEmail();
			}
		}
		
		return closestDeveloper;
	}
	
	/**
	 * Finds the longest shortest path distance from the source developer to another
	 * developer in a set
	 * @param sourceDeveloper
	 * 		The source developer
	 * @param developerSet
	 * 		The developer set
	 * @return
	 * 		An object that maps the name of the source developer to the longest shortest path
	 * 		distance to it, found by this method
	 */
	private double findLongestShortestPathDistance(Developer sourceDeveloper, Map<String, Developer> developerSet) {
		//String farthestDeveloper = null;
		double longestLength = -1;
		Set<String> developerEmails = developerSet.keySet();
		for (String developerEmail : developerEmails) {
			Developer targetDeveloper = developerSet.get(developerEmail);
			DijkstraShortestPath<Developer, DefaultWeightedEdge> shortestPathObject = new DijkstraShortestPath<Developer, DefaultWeightedEdge>(socialGraph, sourceDeveloper, targetDeveloper);
			double pathLength = shortestPathObject.getPathLength();
			if (longestLength < 0 || pathLength > longestLength) {
				longestLength = pathLength;
				//farthestDeveloper = targetDeveloper.getEmail();
			}
		}
		
		//Map<String, Double> output = new HashMap<String, Double>();
		//output.put(sourceDeveloper.getEmail(), longestLength);
		return longestLength;
	}
	
	/**
	 * Find the smallest distance among the given double values in shortestPathDistances (i.e., R_isj
	 * in the Homework 1 answer)
	 * @param shortestPathDistances
	 * 		The mapping from developer to distance
	 * @return
	 * 		The email of the dev with the smallest distance
	 */
	private String developerWithSmallestDistance(Map<String, Double> shortestPathDistances) {
		String dev = null;
		double shortestDistance = -1;
		Set<String> emails = shortestPathDistances.keySet();
		for (String email : emails) {
			if (shortestDistance < 0 || shortestPathDistances.get(email) < shortestDistance) {
				shortestDistance = shortestPathDistances.get(email);
				dev = email;
			}
		}
		
		return dev;
	}
	
	/**
	 * Find the greatest distance among the given double values in shortestPathDistances (i.e., R_isj
	 * in the Homework 1 answer)
	 * @param shortestPathDistances
	 * 		The mapping from developer to distance
	 * @return
	 * 		The email of the dev with the greatest distance
	 */
	private String developerWithGreatestDistance(Map<String, Double> shortestPathDistances) {
		String dev = null;
		double greatestDistance = -1;
		Set<String> emails = shortestPathDistances.keySet();
		for (String email : emails) {
			if (greatestDistance < 0 || shortestPathDistances.get(email) > greatestDistance) {
				greatestDistance = shortestPathDistances.get(email);
				dev = email;
			}
		}
		
		return dev;
	}
	
	/**
	 * Create a file in the temporary folder with the given text
	 * @param filename
	 * 		Name of the file
	 * @param text
	 * 		Content to be written on the file
	 */
	private void createFile(String filename, String text) {
		PrintWriter file = null;
		String filepath = this.pathToTempFolder + "/" + filename;
		try {
			file = new PrintWriter(filepath);
			file.write(text);
			System.out.println("Created text file " + filename);
		} catch (FileNotFoundException e) {
			System.err.println("Error creating" + filename + "file");
		} finally {
			if (file != null) {
				file.close();
			}
		}
	}
}