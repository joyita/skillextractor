package com.project.cpsc534l.skills;

import java.util.List;

/**
 * A subclass of the Skill class, which has the additional
 * grade property.
 * 
 * @author nowrin
 *
 */
public class GradedSkill extends Skill {
	
	private int grade;
	private double normalizedGrade;

	public GradedSkill(String _identifier, List<String> _keywords, int _grade) {
		super(_identifier, _keywords);
		grade = _grade;
		normalizedGrade = 0;
	}
	
	/**
	 * Retrieves the grade for this skill
	 * @return
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * Retrieves the grade for this skill
	 * @return
	 */
	public double getNormalizedGrade() {
		return normalizedGrade;
	}

	/**
	 * @param normalizedGrade
	 */
	public void setNormalizedGrade(double normalizedGrade) {
		this.normalizedGrade = normalizedGrade;
	}	
}
