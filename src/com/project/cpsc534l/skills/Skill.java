package com.project.cpsc534l.skills;

import java.util.List;

/**
 * A skill, which is represented by a list of keywords, and an
 * identifier for the topic
 * 
 * @author focarizajr
 *
 */
public class Skill {
	private String identifier;
	private List<String> keywords;
	
	public Skill(String _identifier, List<String> _keywords) {
		identifier = _identifier;
		keywords = _keywords;
	}
	
	/**
	 * Retrieves the identifier for this skill
	 * @return
	 */
	public String getIdentifier() {
		return identifier;
	}
	
	/**
	 * Retrieves the list of keywords for this skill
	 * @return
	 */
	public List<String> getKeywords() {
		return keywords;
	}
	
	/**
	 * Adds keyword to the list of keywords for this skill
	 * @param keyword
	 * 		The keyword to add
	 */
	public void addKeyword(String keyword) {
		keywords.add(keyword);
	}
}