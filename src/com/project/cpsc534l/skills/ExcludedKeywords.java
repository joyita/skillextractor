package com.project.cpsc534l.skills;

import java.util.ArrayList;
import java.util.List;

public class ExcludedKeywords {
	private static List<String> excludedKeywords;
	
	private static void addKeywords() {
		excludedKeywords = new ArrayList<String>();
		excludedKeywords.add("duplicate");
		excludedKeywords.add("fixed");
		excludedKeywords.add("bug");
		excludedKeywords.add("mozilla");
		excludedKeywords.add("marked");
		excludedKeywords.add("open");
		excludedKeywords.add("close");
		excludedKeywords.add("verified");
		excludedKeywords.add("patch");
		excludedKeywords.add("checked");
		excludedKeywords.add("blocker");
		excludedKeywords.add("fix");
	}
	
	public static List<String> getKeywords() {
		addKeywords();
		return excludedKeywords;
	}
}