package com.project.cpsc534l.skills.extractor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.project.cpsc534l.skills.GradedSkill;

/**
 * Developer class which contains the list of graded skills for a particular developer
 * @author nowrin
 *
 */
public class Developer {
	private String email;
	/**
	 *  A single string containing all the summary and description of 
	 *  the bug reports that are assigned to this developer and comments 
	 *  on those bug reports made only by this developer 
	 */
	private String comments;
	private List<BugReport> bugReports;
	private Map<String, GradedSkill> skills;
	
	/**
	 * @param _email
	 * 		email of the developer
	 */
	public Developer(String _email) {
		email = _email;
		comments = "";
		bugReports = new ArrayList<BugReport>();
		skills = new HashMap<String, GradedSkill>();
	}

	/**
	 * Retrieves the email for this developer
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Retrieves the comments made by this developer
	 * @return
	 */
	public String getComments() {
		return comments;
	}
	
	/**
	 * Set the comments made by this developer
	 * @param comments
	 * 		The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	/**
	 * Retrieves the list of bugReports assigned for this developer
	 * @return
	 */
	public List<BugReport> getBugReports() {
		return bugReports;
	}
	
	/**
	 * Adds bugReport to the list of bugReports for this developer
	 * @param bugReport
	 * 		The bugReport to add
	 */
	public void addBugReport(BugReport bugReport) {
		bugReports.add(bugReport);
	}

	/**
	 * Retrieves the map of graded skills for this developer
	 * @return
	 */
	public Map<String, GradedSkill> getSkills() {
		return skills;
	}
	
	/**
	 * Adds skill to the map of skills for this developer
	 * @param id
	 * 		id of the skill to add, which will be the key to the map
	 * @param skill
	 * 		The skill to add
	 */
	public void addSkill(String id, GradedSkill skill) {
		skills.put(id, skill);
	}
}
