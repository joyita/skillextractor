package com.project.cpsc534l.skills.extractor;

/**
 * A bug report comment
 * @author focarizajr
 *
 */
public class BugReportComment {
	private String text;
	private String commenter;
	
	/**
	 * The constructor
	 * @param _text
	 * 		The bug report's raw text
	 * @param _commenter
	 * 		The name (or e-mail) of the person that commented
	 * 		on the bug report
	 */
	public BugReportComment(String _text, String _commenter) {
		text = _text;
		commenter = _commenter;
	}
	
	/**
	 * Retrieve the raw text of the bug report
	 * @return
	 * 		The bug report's raw text
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Retrieve the commenter's name
	 * @return
	 * 		The commenter's name
	 */
	public String getCommenter() {
		return commenter;
	}
}