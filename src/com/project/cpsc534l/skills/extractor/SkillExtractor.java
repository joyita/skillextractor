package com.project.cpsc534l.skills.extractor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.project.cpsc534l.skills.ExcludedKeywords;
import com.project.cpsc534l.skills.Skill;
import com.project.cpsc534l.utils.JsonUrlExtractor;

/**
 * Extracts the skills from a list of bug reports. This requires the following
 * inputs:
 * 
 * 1. The URL to the list of bug reports (in JSON format)
 * 2. The prefix and suffix of the URL for the bug report comments JSON file
 * 		(The url is assumed to be of the form <prefix> + <bug-report-id> + <suffix>
 * 3. The number of skills to extract
 * 4. The path to a temporary folder, which must contain the following:
 * 		a. A folder named "bug_report_files" where .txt inputs to mallet will be temporarily stored
 * 		b. A folder named "mallet_files" where mallet output files will be temporarily placed
 * 5. The mallet command for your platform
 * 6. The maximum number of bug reports to consider
 * 
 * @author focarizajr
 *
 */
public class SkillExtractor {
	private String bugReportsListUrl;
	private String bugReportCommentsUrlPrefix;
	private String bugReportCommentsUrlSuffix;
	private int numSkills;
	private String pathToTempFolder;
	private String malletCmd;
	private int maxNumBugReports;

	private List<Skill> skills;
	private List<BugReport> bugReports;

	final private String MALLET_FILE = "bug_reports.mallet";
	final private String MALLET_OUTPUT_FILE = "topics.txt";
	final private String OUTPUT_STATE_FILE = "topics.gz";
	final private String STOP_WORDS_FILE = "extra_stopwords.txt";

	/**
	 * The constructor
	 * @param _url
	 * 		The URL to the list of bug reports
	 * @param _urlPrefix
	 * 		The prefix of the URL for the bug report comments
	 * @param _urlSuffix
	 * 		The suffix of the URL for the bug report comments
	 * @param _numTopics
	 * 		The number of topics/skills to extract
	 * @param _pathToTempFolder
	 * 		The path to the temporary folder. Must contain folders called "bug_report_files" and "mallet_files"
	 * @param _malletCmd
	 * 		The mallet command
	 * @param _maxNumBugReports
	 * 		The maximum number of bug reports to consider
	 */
	public SkillExtractor(String _url, String _urlPrefix, String _urlSuffix, int _numSkills, String _pathToTempFolder, String _malletCmd, int _maxNumBugReports) {
		bugReportsListUrl = _url;
		bugReportCommentsUrlPrefix = _urlPrefix;
		bugReportCommentsUrlSuffix = _urlSuffix;
		numSkills = _numSkills;
		pathToTempFolder = _pathToTempFolder;
		malletCmd = _malletCmd;
		maxNumBugReports = _maxNumBugReports;

		skills = new ArrayList<Skill>();
		bugReports = new ArrayList<BugReport>();
	}

	/**
	 * Extracts the list of skills from a set of bug reports
	 * @return
	 */
	public void extractSkills() {
		try {
			//Retrieve the JSON object containing the list of bug reports
			JsonUrlExtractor jsonExtractor = new JsonUrlExtractor(bugReportsListUrl);

			System.out.println("Retrieved JSON of bug reports");

			JSONObject bugReportsJson = jsonExtractor.getJsonObj();

			//Create a BugReport object for each bug report in the list
			JSONArray bugReportsArray = bugReportsJson.getJSONArray("bugs");
			extractBugReports(bugReportsArray);

			if (bugReports.isEmpty()) {
				System.err.println("No bug reports extracted!");
				return;
			}

			//Create .txt files for each bug report and store 
			//them in <pathToTempFolder>/bug_report_files
			generateMalletTextFiles();

			//Run mallet on the .txt files just created
			runMallet();
		} catch (JSONException e) {
			System.err.println("Problem with parsing bug reports JSON in SkillExtractor");
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println("Problem with retrieving bug reports JSON from URL in SkillExtractor");
		} catch (InterruptedException e) {
			System.err.println("Problem with mallet process");
		}
	}

	/**
	 * Retrieves the list of bug reports
	 * @return
	 */
	public List<BugReport> getBugReports() {
		return bugReports;
	}

	/**
	 * Retrieves the list of skills
	 * @return
	 */
	public List<Skill> getSkills() {
		return skills;
	}

	/**
	 * Creates a bug report object for every bug report in the list
	 * @param bugReportsArray
	 * 		JSON object containing data for the bug reports
	 * @throws IOException
	 */
	private void extractBugReports(JSONArray bugReportsArray) {
		int numReportsToConsider = bugReportsArray.length();
		for (int i = 0; i < numReportsToConsider; i++) {
			JSONObject nextReportJson = bugReportsArray.getJSONObject(i);
			try {
				int id = nextReportJson.getInt("id"); //Grab the bug report's ID
				String summary = nextReportJson.getString("summary").trim();
				String assignee = nextReportJson.getString("assigned_to");
				BugReport nextReport = new BugReport(new ArrayList<BugReportComment>(), id, summary, assignee);

				//Grab the list of comments for the current bug report
				JsonUrlExtractor jsonExtractor = new JsonUrlExtractor(bugReportCommentsUrlPrefix + id + bugReportCommentsUrlSuffix);
				JSONObject commentsJson = jsonExtractor.getJsonObj();

				//Create a BugReportComment object for each comment (note how id is implicitly converted to string)
				JSONArray commentsArray = commentsJson.getJSONObject("bugs").getJSONObject("" + id).getJSONArray("comments");
				for (int j = 0; j < commentsArray.length(); j++) {
					JSONObject nextCommentJson = commentsArray.getJSONObject(j);
					try {
						String commenter = nextCommentJson.getString("creator").trim();
						String comment_text = nextCommentJson.getString("raw_text").trim();
						if (!commenter.equals("") && !comment_text.equals("")) {
							BugReportComment nextComment = new BugReportComment(comment_text, commenter);
							nextReport.addComment(nextComment);
						}
					}
					catch (JSONException e) {
						//Ignore comment if there is a JSON exception for this comment
						System.out.println("Ignoring comment for bug report #" + id + ": Can't parse JSON");
					}
				}

				if (!nextReport.getComments().isEmpty() && !nextReport.getSummary().equals("")) {
					System.out.println("Processed bug report #" + i + " of " + numReportsToConsider);
					bugReports.add(nextReport);
				}
			}
			catch (JSONException e) {
				//Ignore bug report if there is a JSON exception for this bug report
				System.out.println("Ignoring bug report: Can't parse JSON");
			}
			catch (IOException e) {
				//Ignore bug report if there is an IO exception when retrieving the URL
				System.out.println("Ignoring bug report: Can't retrieve JSON comments URL");
			}
		}		
	}

	/**
	 * Generates the mallet .txt files based on the bug reports
	 */
	private void generateMalletTextFiles() {
		int numReportsToConsider = bugReports.size();
		if (numReportsToConsider > maxNumBugReports) {
			numReportsToConsider = maxNumBugReports;
		}
		for (int i = 0; i < numReportsToConsider; i++) {
			BugReport nextBugReport = bugReports.get(i);
			List<BugReportComment> comments = nextBugReport.getComments();

			String commentsText = nextBugReport.getSummary();
			for (int j = 0; j < comments.size(); j++) {
				BugReportComment nextComment = comments.get(j);
				String text = nextComment.getText();
				commentsText += "\n\n" + text;
			}

			//Create .txt files containing the comments
			PrintWriter file = null;
			String filename = this.pathToTempFolder + "/bug_report_files/bug_report" + nextBugReport.getId() + ".txt";
			try {
				file = new PrintWriter(filename);
				file.write(commentsText);
				System.out.println("Created file for Bug Report #" + i + " of " + bugReports.size());
			} catch (FileNotFoundException e) {
				System.err.println("Error creating bug report #" + nextBugReport.getId() + " file");
			} finally {
				if (file != null) {
					file.close();
				}
			}
		}

		//Create additional stopwords (i.e., words to exclude)
		List<String> stopWordsList = ExcludedKeywords.getKeywords();
		String stopWordsText = "";
		for (int i = 0; i < stopWordsList.size(); i++) {
			stopWordsText += stopWordsList.get(i) + " ";
		}
		PrintWriter file = null;
		String filename = this.pathToTempFolder + "/mallet_files/" + STOP_WORDS_FILE;
		try {
			file = new PrintWriter(filename);
			file.write(stopWordsText);
			System.out.println("Created extra stopwords file");
		} catch (FileNotFoundException e) {
			System.err.println("Error creating extra stopwords file");
		} finally {
			if (file != null) {
				file.close();
			}
		}
	}

	/**
	 * Runs mallet to generate the skills
	 * @throws InterruptedException 
	 */
	private void runMallet() throws InterruptedException {
		try {
			//Run mallet on the .txt files just created
			ProcessBuilder pb = new ProcessBuilder(malletCmd, "import-dir", "--input", pathToTempFolder + "/bug_report_files/", "--output", pathToTempFolder + "/mallet_files/" + MALLET_FILE, "--keep-sequence", "--remove-stopwords", "--extra-stopwords", pathToTempFolder + "/mallet_files/" + STOP_WORDS_FILE);
			System.out.println("Generating mallet file...");
			Process p = pb.start();
			p.waitFor();
			System.out.println("Mallet file generated");

			pb = new ProcessBuilder(malletCmd, "train-topics", "--input", pathToTempFolder + "/mallet_files/" + MALLET_FILE, "--num-topics", "" + numSkills, "--output-state", pathToTempFolder + "/mallet_files/" + OUTPUT_STATE_FILE, "--output-topic-keys", pathToTempFolder + "/mallet_files/" + MALLET_OUTPUT_FILE);
			//System.out.println("Training topics: " + malletCmd + " train-topics --input" + " " + pathToTempFolder + "/mallet_files/" + MALLET_FILE + " --num-topics " + numSkills + " --output-state " + pathToTempFolder + "/mallet_files/" + OUTPUT_STATE_FILE + " --output-topic-keys " + pathToTempFolder + "/mallet_files/" + MALLET_OUTPUT_FILE);
			p = pb.start();
			inheritIO(p.getInputStream(), System.out);
			inheritIO(p.getErrorStream(), System.err);
			p.waitFor();
			System.out.println("Training finished");

			//Parse mallet's output, which contains the topics/skills and their keywords
			//The mallet output file is of the form 
			//	<number1>  <number2>  <keywords>
			//at each line. Hence, create a skill for every line, and add the keyword on
			//that line to the corresponding skill
			BufferedReader br = new BufferedReader(new FileReader(pathToTempFolder + "/mallet_files/" + MALLET_OUTPUT_FILE));
			String line;
			while ((line = br.readLine()) != null) {
				StringTokenizer st = new StringTokenizer(line);
				if (st.countTokens() < 3) {
					continue; //Ignore this line since there are no keywords
				}

				String number1 = st.nextToken(); //<number1>
				String number2 = st.nextToken(); //<number2>

				Skill newSkill = new Skill("skill" + number1, new ArrayList<String>());
				while (st.hasMoreTokens()) {
					newSkill.addKeyword(st.nextToken());
				}

				skills.add(newSkill);
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("Can't locate mallet output file");
		} catch (IOException e) {
			System.err.println("Problem starting mallet");
		}
	}

	private static void inheritIO(final InputStream src, final PrintStream dest) {
		new Thread(new Runnable() {
			public void run() {
				Scanner sc = new Scanner(src);
				while (sc.hasNextLine()) {
					dest.println(sc.nextLine());
				}
			}
		}).start();
	}
}