package com.project.cpsc534l.skills.extractor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.project.cpsc534l.skills.GradedSkill;

/**
 * A bug report
 * @author focarizajr
 *
 */
public class BugReport {
	private List<BugReportComment> comments;
	private int id;
	private String summary;
	private String assignee;
	private Map<String, GradedSkill> skills;
	
	private Map<String, Developer> developers; //The bug report team
	
	/**
	 * The constructor
	 * @param _comments
	 * 		The list of comments for this bug report
	 * @param _id
	 * 		This bug report's ID
	 * @param _summary
	 * 		The bug report title/summary
	 * @param _assignee
	 * 		Email of the developer who is assigned to this bug report
	 */
	public BugReport(List<BugReportComment> _comments, int _id, String _summary, String _assignee) {
		comments = _comments;
		id = _id;
		summary = _summary;
		assignee = _assignee;
		skills = new HashMap<String, GradedSkill>();
		
		developers = new HashMap<String, Developer>();
	}
	
	/**
	 * Adds a comment to the list of bug reports
	 * @param comment
	 */
	public void addComment(BugReportComment comment) {
		comments.add(comment);
	}
	
	/**
	 * Retrieves the list of comments for this bug report
	 * @return
	 */
	public List<BugReportComment> getComments() {
		return comments;
	}
	
	/**
	 * Retrieves this bug report's ID
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Retrieves the summary/title of this bug report
	 * @return
	 */
	public String getSummary() {
		return summary;
	}
	
	/**
	 * Retrieves the assignee of this bug report
	 * @return
	 */
	public String getAssignee() {
		return assignee;
	}
	
	/**
	 * Retrieves the map of graded skills for this bug report
	 * @return
	 */
	public Map<String, GradedSkill> getSkills() {
		return skills;
	}
	
	/**
	 * Retrieves the map of developers (i.e., the bug report team)
	 * for this bug report
	 * @return
	 */
	public Map<String, Developer> getDevelopers() {
		return developers;
	}
	
	/**
	 * Adds skill to the map of skills for this bug report
	 * @param id
	 * 		id of the skill to add, which will be the key to the map
	 * @param skill
	 * 		The skill to add
	 */
	public void addSkill(String id, GradedSkill skill) {
		skills.put(id, skill);
	}
	
	/**
	 * Adds developer to the map of developers for this bug report
	 * @param id
	 * 		id of the developer to add, which will be the key to the map
	 * @param developer
	 * 		The developer to add
	 */
	public void addDeveloper(String id, Developer developer) {
		developers.put(id, developer);
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). //two randomly chosen prime numbers
			append(id).
			toHashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof BugReport)) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		
		BugReport br = (BugReport) obj;
		return new EqualsBuilder().
			append(id, br.id).
			isEquals();
	}
}