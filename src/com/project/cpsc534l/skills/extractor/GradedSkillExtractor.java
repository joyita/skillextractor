package com.project.cpsc534l.skills.extractor;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.project.cpsc534l.skills.GradedSkill;
import com.project.cpsc534l.skills.Skill;

/**
 * Extracts the graded skills for developers from a list of bug reports 
 * and a list of skills. This requires the following inputs:
 * 
 * 1. Path to a temporary folder
 * 2. The maximum number of bug reports to consider for the train set
 * 3. The list of Skill objects
 * 4. The list of BugReport objects
 * 
 * @author nowrin
 *
 */
public class GradedSkillExtractor {
	private String pathToTempFolder;
	private int maxNumBugReports;

	private List<Skill> skills;
	private List<BugReport> bugReports;
	private Map<String, Developer> developers;

	final private String EXCLUDED_DEVELOPER = "nobody@mozilla.org";

	/**
	 * 
	 * @param _pathToTempFolder
	 * 		Path to the temporary folder
	 * @param _maxNumBugReports
	 * 		The maximum number of bug reports to consider for the train set
	 * @param _skills
	 * 		The list of Skill objects
	 * @param _bugReports
	 * 		The list of BugReport objects
	 */
	public GradedSkillExtractor(String _pathToTempFolder, int _maxNumBugReports, List<Skill> _skills, List<BugReport> _bugReports) {
		pathToTempFolder = _pathToTempFolder;
		maxNumBugReports = _maxNumBugReports;

		skills = _skills;
		bugReports = _bugReports;
		developers = new HashMap<String, Developer>();
	}

	/**
	 * Extracts the list of graded skills for each developer.
	 * @return
	 */
	public void extractSkillsForDevelopers() {
		extractDevelopers();

		if (developers.size() > 0) {
			Set<String> developerEmails = developers.keySet();
			for (String developerEmail : developerEmails) {
				Developer developer = developers.get(developerEmail);
				String comments = developer.getComments();

				for (Skill skill : skills) {
					int grade = calculateGrade(skill.getKeywords(), comments);

					GradedSkill gradedSkill = new GradedSkill (skill.getIdentifier(), skill.getKeywords(), grade);
					developer.addSkill(skill.getIdentifier(), gradedSkill);
					System.out.println("Added grade " + grade + " to " + skill.getIdentifier() + " for developer " + developerEmail);
				}

				System.out.println("Updated developer " + developerEmail + " in the map with the grade");
			}

			for (Skill skill : skills) {
				int sum = 0;
				for (String developerEmail : developerEmails) {
					Developer developer = developers.get(developerEmail);
					GradedSkill gradedSkill = developer.getSkills().get(skill.getIdentifier());
					if (gradedSkill != null) {
						sum += gradedSkill.getGrade();
					}
				}

				double average = (double) sum/developerEmails.size();

				for (String developerEmail : developerEmails) {
					Developer developer = developers.get(developerEmail);
					GradedSkill gradedSkill = developer.getSkills().get(skill.getIdentifier());
					if (gradedSkill != null) {
						calculateNormalizedGrade(average, gradedSkill);
					}
				}

				System.out.println("Normalized grades for " + skill.getIdentifier() + " for all the developers");
			}

			generateTextFileForDevelopers();
		}
	}	

	/**
	 * Extracts the list of graded skills for each bug reports in the test set.
	 * @return
	 */
	public void extractSkillsForTestSet() {
		if (bugReports.size() > maxNumBugReports) {
			for (int i = maxNumBugReports; i < bugReports.size(); i++) {
				BugReport nextBugReport = bugReports.get(i);
				String text = nextBugReport.getSummary();

				for (BugReportComment comment : nextBugReport.getComments()) {
					text += comment.getText();
				}

				for (Skill skill : skills) {
					int grade = calculateGrade(skill.getKeywords(), text);

					GradedSkill gradedSkill = new GradedSkill (skill.getIdentifier(), skill.getKeywords(), grade);
					nextBugReport.addSkill(skill.getIdentifier(), gradedSkill);
					System.out.println("Added grade " + grade + " to " + skill.getIdentifier() + " for bug report " + nextBugReport.getId());
				}

				System.out.println("Updated bug report " + nextBugReport.getId() + " in the map with the grade");
			}

			for (Skill skill : skills) {
				int sum = 0;
				for (int j = maxNumBugReports; j < bugReports.size(); j++) {
					BugReport nextBugReport = bugReports.get(j);
					GradedSkill gradedSkill = nextBugReport.getSkills().get(skill.getIdentifier());
					if (gradedSkill != null) {
						sum += gradedSkill.getGrade();
					}
				}

				double average = (double) sum/(bugReports.size() - maxNumBugReports);

				for (int k = maxNumBugReports; k < bugReports.size(); k++) {
					BugReport nextBugReport = bugReports.get(k);
					GradedSkill gradedSkill = nextBugReport.getSkills().get(skill.getIdentifier());
					if (gradedSkill != null) {
						calculateNormalizedGrade(average, gradedSkill);
					}
				}

				System.out.println("Normalized grades for " + skill.getIdentifier() + " for all the test set");
			}
			
//			generateTextFileForTestSet();
		}
	}

	/**
	 * Retrieves the map of developers, which maps the Developer 
	 * instance associated with each developer
	 * @return
	 */
	public Map<String, Developer> getDevelopers() {
		return developers;
	}

	/**
	 * Creates a Developer object for each developer
	 * present in the train set of the bug reports
	 */
	private void extractDevelopers() {
		int numReportsToConsider = bugReports.size();
		if (numReportsToConsider > maxNumBugReports) {
			numReportsToConsider = maxNumBugReports;
		}
		for (int i = 0; i < numReportsToConsider; i++) {
			BugReport nextBugReport = bugReports.get(i);
			String assignee = nextBugReport.getAssignee();
			List<BugReportComment> comments = nextBugReport.getComments();

			if (!assignee.equals(EXCLUDED_DEVELOPER)) {				
				/* 
				 * This is the string that will contain the summary and description 
				 * of this bug report and all the comments made only by the assignee 
				 * of this bug report
				 */
				String commentsByAssignee = nextBugReport.getSummary();

				for (int j = 0; j < comments.size(); j++) {
					BugReportComment nextComment = comments.get(j);
					String commenter = nextComment.getCommenter();

					/* 
					 * The first comment in a bug report is the description of the 
					 * bug report, which will always be added
					 */				
					if (commenter.equals(assignee) || j == 0) {
						commentsByAssignee += "\n\n" + nextComment.getText();
					}
				}

				/*
				 * If the assignee is already in the developers map, then append 
				 * the previous comments with the current comments,
				 * otherwise put the assignee as a new key in the map.
				 * Update the comments in the mapped Developer instance, also add 
				 * the bug report to the mapped Developer's bugReports list.
				 */
				if (developers.containsKey(assignee)) {
					commentsByAssignee += "\n\n" + developers.get(assignee).getComments();										
				} else {
					developers.put(assignee, new Developer(assignee));
				}
				developers.get(assignee).setComments(commentsByAssignee);
				developers.get(assignee).addBugReport(nextBugReport);
			}
		}
	}

	/**
	 * 
	 * @param keywords
	 * 		keywords present in a skill set
	 * @param text
	 * 		String which will be searched for the keywords
	 * The grade is the sum of the number of times each keyword 
	 * is present in a string.
	 * @return
	 */
	private int calculateGrade(List<String> keywords, String text) {
		String patternString = "\\b(" + StringUtils.join(keywords, "|") + ")\\b";
		Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(text);

		int grade = 0;
		while (matcher.find()) {
			grade++;
		}
		return grade;
	}

	/**
	 * Calculates the normalized grade. 
	 * If the grade is greater than or equal to twice the average, normalized grade is set to 1.0.
	 * Otherwise it is normalized by dividing it by twice the average.
	 */
	private void calculateNormalizedGrade(double average, GradedSkill gradedSkill) {
		if (gradedSkill.getGrade() >= (2*average)) {
			gradedSkill.setNormalizedGrade(1.0);
		} else {
			double normalizedGrade = gradedSkill.getGrade()/(2*average);
			gradedSkill.setNormalizedGrade(new BigDecimal(normalizedGrade).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue());
		}
	}

	/**
	 * Generates the developers.txt file based on the graded skills of all the developers
	 */
	private void generateTextFileForDevelopers() {
		if (developers.size() > 0) {
			String text = "";

			Set<String> developerEmails = developers.keySet();
			for (String developerEmail : developerEmails) {
				Developer developer = developers.get(developerEmail);

				text += developerEmail + "\t";
				Set<String> skillIds = developer.getSkills().keySet();
				for (String skillId : skillIds) {
					GradedSkill skill = developer.getSkills().get(skillId);
					text += skill.getIdentifier() + ":" + skill.getGrade() + " (" + skill.getNormalizedGrade() + ")" + "\t";
				}
				text += "\n";
			}

			//Create .txt file containing the graded skills for all the developers
			createFile("developers.txt", text);
		}
	}

	/**
	 * Generates the bugreports.txt file based on the graded skills of all the bug reports in the test set
	 */
	private void generateTextFileForTestSet() {
		if (bugReports.size() > maxNumBugReports) {
			String text = "";
			for (int i = maxNumBugReports; i < bugReports.size(); i++) {
				BugReport nextBugReport = bugReports.get(i);

				text += "Bug ID: " + nextBugReport.getId() + "\t";
				Set<String> skillIds = nextBugReport.getSkills().keySet();
				for (String skillId : skillIds) {
					GradedSkill skill = nextBugReport.getSkills().get(skillId);
					text += skill.getIdentifier() + ":" + skill.getGrade() + " (" + skill.getNormalizedGrade() + ")" + "\t";
				}
				text += "\n";
			}

			//Create .txt file containing the graded skills for all the bug reports
			createFile("bugreports.txt", text);
		}
	}

	/**
	 * Create a file in the temporary folder with the given text
	 * @param filename
	 * 		Name of the file
	 * @param text
	 * 		Content to be written on the file
	 */
	private void createFile(String filename, String text) {
		PrintWriter file = null;
		String filepath = this.pathToTempFolder + "/" + filename;
		try {
			file = new PrintWriter(filepath);
			file.write(text);
			System.out.println("Created text file " + filename);
		} catch (FileNotFoundException e) {
			System.err.println("Error creating" + filename + "file");
		} finally {
			if (file != null) {
				file.close();
			}
		}
	}
}
