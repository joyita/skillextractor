package com.project.cpsc534l.testrunners;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import com.project.cpsc534l.skills.Skill;
import com.project.cpsc534l.skills.extractor.Developer;
import com.project.cpsc534l.skills.extractor.GradedSkillExtractor;
import com.project.cpsc534l.skills.extractor.SkillExtractor;
import com.project.cpsc534l.social.SocialGraphMaker;
import com.project.cpsc534l.team.TeamFinder;

/**
 * Test runner
 * @author nowrin
 *
 */
public class TestRunner {
	public static void main(String args[]) {
		if (args.length < 4) {
			System.out.println("Correct arguments: <malletCmd> <pathToTempFolder> <numSkills> <maxNumBugReports> <startIndex> <endIndex>");
			return;
		}

		String malletCmd = args[0];
		String pathToTempFolder = args[1];
		int numSkills = Integer.parseInt(args[2]);
		int maxNumBugReports = Integer.parseInt(args[3]);
		int startIndex = Integer.parseInt(args[4]);
		int endIndex = Integer.parseInt(args[5]);

		String url = "https://bugzilla.mozilla.org/rest/bug?include_fields=id,summary,assigned_to,status&limit=0&order=priority%2Cbug_severity&resolution=FIXED";
		String urlPrefix = "https://bugzilla.mozilla.org/rest/bug/";
		String urlSuffix = "/comment";

		SkillExtractor skillExtractor = new SkillExtractor(url, urlPrefix, urlSuffix, numSkills, pathToTempFolder, malletCmd, maxNumBugReports);

		skillExtractor.extractSkills();	

		System.out.println("Number of skills: " + skillExtractor.getSkills().size());

		int numReportsToConsider = skillExtractor.getSkills().size();
		if (numReportsToConsider > maxNumBugReports) {
			numReportsToConsider = maxNumBugReports;
		}
		System.out.println("Training Set size: " + numReportsToConsider);
		for (int i = 0; i < numReportsToConsider; i++) {
			Skill nextSkill = skillExtractor.getSkills().get(i);
			System.out.println(nextSkill.getIdentifier() + " has " + nextSkill.getKeywords().size() + " keywords");
		}	


		GradedSkillExtractor gradedSkillExtractor = new GradedSkillExtractor(pathToTempFolder, maxNumBugReports, skillExtractor.getSkills(), skillExtractor.getBugReports());
		gradedSkillExtractor.extractSkillsForDevelopers();
		System.out.println("Number of developers:" + gradedSkillExtractor.getDevelopers().size());

		for (String developerEmail : gradedSkillExtractor.getDevelopers().keySet()) {
			Developer developer = gradedSkillExtractor.getDevelopers().get(developerEmail);
			System.out.println("Developer " + developer.getEmail() + " has " + developer.getBugReports().size() + " bug reports assigned to him");
		}

		gradedSkillExtractor.extractSkillsForTestSet();
		
		SocialGraphMaker socialGraphMaker = new SocialGraphMaker(gradedSkillExtractor.getDevelopers());
		socialGraphMaker.makeSocialGraph();
		
		SimpleWeightedGraph<Developer, DefaultWeightedEdge> socialGraph = socialGraphMaker.getSocialGraph();
		
		TeamFinder teamFinder = new TeamFinder(pathToTempFolder, maxNumBugReports, socialGraph, gradedSkillExtractor.getDevelopers(), skillExtractor.getBugReports(), startIndex, endIndex);
		teamFinder.findTeamForAllBugReports();
	}
}
