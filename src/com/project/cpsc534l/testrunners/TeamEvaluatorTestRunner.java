package com.project.cpsc534l.testrunners;

import java.util.Map;
import java.util.Set;

import com.project.cpsc534l.evaluation.TeamEvaluator;
import com.project.cpsc534l.evaluation.TeamExtractor;

/**
 * @author nowrin
 *
 */
public class TeamEvaluatorTestRunner {
	public static void main(String args[]) {
		String url = "https://bugzilla.mozilla.org/rest/bug?include_fields=id,assigned_to,status&limit=0&order=priority%2Cbug_severity&resolution=FIXED";
		int trainSetSize = 1000;
		String filename = "/Users/mahzabin/Documents/workspace/SkillExtractor/temp/test.txt" ;
		
		TeamExtractor extractor = new TeamExtractor(url, trainSetSize, filename);
		Map<String, Set<String>> actualTeams = extractor.extractActualTeams();
		Map<String, Set<String>> recommendedTeams = extractor.extractRecommendedTeams();
		
		TeamEvaluator teamEvaluator = new TeamEvaluator(actualTeams, recommendedTeams);
		Map<String, Double> jaccardCoffs = teamEvaluator.evaluate();

		for (String bugID : jaccardCoffs.keySet()) {
			System.out.println("BugID: " + bugID + " JaccardCoffs: " + jaccardCoffs.get(bugID));
		}
	}

	
}
