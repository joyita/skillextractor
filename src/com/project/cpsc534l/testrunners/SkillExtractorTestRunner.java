package com.project.cpsc534l.testrunners;

import com.project.cpsc534l.skills.Skill;
import com.project.cpsc534l.skills.extractor.SkillExtractor;

/**
 * Test runner for SkillExtractor
 * @author focarizajr
 *
 */
public class SkillExtractorTestRunner {
	public static void main(String args[]) {
		if (args.length < 4) {
			System.out.println("Correct arguments: <malletCmd> <pathToTempFolder> <numSkills> <maxNumBugReports>");
			return;
		}
		
		String malletCmd = args[0];
		String pathToTempFolder = args[1];
		int numSkills = Integer.parseInt(args[2]);
		int maxNumBugReports = Integer.parseInt(args[3]);
		
		String url = "https://bugzilla.mozilla.org/rest/bug?include_fields=id,summary,assigned_to,status&limit=0&order=priority%2Cbug_severity&resolution=FIXED";
		String urlPrefix = "https://bugzilla.mozilla.org/rest/bug/";
		String urlSuffix = "/comment";
		
		SkillExtractor skillExtractor = new SkillExtractor(url, urlPrefix, urlSuffix, numSkills, pathToTempFolder, malletCmd, maxNumBugReports);
		
		skillExtractor.extractSkills();	
		
		System.out.println("Number of skills: " + skillExtractor.getSkills().size());
		for (int i = 0; i < skillExtractor.getSkills().size(); i++) {
			Skill nextSkill = skillExtractor.getSkills().get(i);
			System.out.println(nextSkill.getIdentifier() + " has " + nextSkill.getKeywords().size() + " keywords");
		}	
	}
}