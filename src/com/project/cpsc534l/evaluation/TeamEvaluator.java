package com.project.cpsc534l.evaluation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * TeamEvaluator class evaluates the actual teams and the recommended teams
 * using the jaccard coefficient, 
 * J(A, B) = |A intersection B| / |A union B|
 * @author nowrin
 *
 */
public class TeamEvaluator {
	
	private Map<String, Set<String>> actualTeams;
	private Map<String, Set<String>> recommendedTeams;
	
	public TeamEvaluator(Map<String, Set<String>> _actualTeams, Map<String, Set<String>> _recommendedTeams) {
		actualTeams = _actualTeams;
		recommendedTeams = _recommendedTeams;
	}
	
	/**
	 * Returns a map with the BugID and their mapped jaccard coefficient
	 * @return
	 */
	public Map<String, Double> evaluate() {
		Map<String, Double> jaccardCoefficients = new HashMap<String, Double>();
		
		for (String BugID : actualTeams.keySet()){
			Set<String> actualTeam = actualTeams.get(BugID);
			Set<String> recommendedTeam = recommendedTeams.get(BugID);
			
			Set<String> union = new HashSet<String>(actualTeam);
			union.addAll(recommendedTeam);
			
			Set<String> intersection = new HashSet<String>(actualTeam);
			intersection.retainAll(recommendedTeam);
			
			double jaccardCoefficient = (double) intersection.size() / union.size();
			
			jaccardCoefficients.put(BugID, new Double(jaccardCoefficient));
		}
		
		return jaccardCoefficients;
	}
}
