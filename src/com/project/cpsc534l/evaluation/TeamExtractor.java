package com.project.cpsc534l.evaluation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.project.cpsc534l.utils.JsonUrlExtractor;

/**
 * @author nowrin
 *
 */
public class TeamExtractor {
	
	private String bugReportURL;
	private int trainSetSize;
	private String recommendedTeamsFilename;

	public TeamExtractor(String _bugReportURL, int _trainSetSize, String _recommendedTeamsFilename) {
		bugReportURL = _bugReportURL;
		trainSetSize = _trainSetSize;
		recommendedTeamsFilename = _recommendedTeamsFilename;
	}
	
	public Map<String, Set<String>> extractActualTeams() {
		Map<String, Set<String>> actualTeams = new HashMap<String, Set<String>>();

		try {
			//Retrieve the JSON object containing the list of bug reports
			JsonUrlExtractor jsonExtractor = new JsonUrlExtractor(bugReportURL);
			JSONObject bugReportsJson = jsonExtractor.getJsonObj();

			//Create a BugReport object for each bug report in the list
			JSONArray bugReportsArray = bugReportsJson.getJSONArray("bugs");
			if (bugReportsArray.length() > trainSetSize) {
				for (int i = trainSetSize; i < bugReportsArray.length(); i++) {
					JSONObject nextReportJson = bugReportsArray.getJSONObject(i);

					String bugID = "" + nextReportJson.getInt("id"); 
					String assignee = nextReportJson.getString("assigned_to");
					
					Set<String> actualTeamMembers = new HashSet<String>();
					actualTeamMembers.add(assignee);
					
					//Grab the list of commenters for the current bug report
					String bugReportCommentsUrlPrefix = "https://bugzilla.mozilla.org/rest/bug/";
					String bugReportCommentsUrlSuffix = "/comment?include_fields=creator";
					JsonUrlExtractor jsoncommentExtractor = new JsonUrlExtractor(bugReportCommentsUrlPrefix + bugID + bugReportCommentsUrlSuffix);
					
					JSONObject commentsJson = jsoncommentExtractor.getJsonObj();
					JSONArray commentsArray = commentsJson.getJSONObject("bugs").getJSONObject("" + bugID).getJSONArray("comments");
					for (int j = 0; j < commentsArray.length(); j++) {
						JSONObject nextCommentJson = commentsArray.getJSONObject(j);
						String commenter = nextCommentJson.getString("creator").trim();
						if (!commenter.equals("") && !commenter.equals("nobody@mozilla.org")) {
							actualTeamMembers.add(commenter);						
						}
					}
					
					actualTeams.put(bugID, actualTeamMembers);
				}
			}

		} catch (JSONException e) {
			System.err.println("Problem with parsing JSON");
			System.err.println(e.getMessage());
		} catch (IOException e) {
			System.err.println("Problem with retrieving JSON from URL");
			System.err.println(e.getMessage());
		} 

		return actualTeams;
	}

	public Map<String, Set<String>> extractRecommendedTeams() {
		Map<String, Set<String>> recommendedTeams = new HashMap<String, Set<String>>();

		try {
			InputStream in = new FileInputStream(recommendedTeamsFilename);		
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line = null;
			while( (line=reader.readLine() ) != null)
			{
				//Skip empty lines
				if(line.trim().length() == 0 ) {
					continue;
				}

				String[] fields = line.split(" ");
				if (fields.length > 1) {
					String bugID = fields[0];
					Set<String> recommendedTeamMembers = new HashSet<String>();
					for (int i = 1; i < fields.length; i++) {
						recommendedTeamMembers.add(fields[i]);
					}

					recommendedTeams.put(bugID, recommendedTeamMembers);
				}
			}

			reader.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}	
		catch (IOException e) {
			e.printStackTrace();
		}	

		return recommendedTeams;
	}
}
