package com.project.cpsc534l.social;

import java.util.Iterator;
import java.util.Set;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import com.project.cpsc534l.skills.extractor.Developer;

/**
 * Social graph for bug report testers
 * @author focarizajr
 *
 */
public class SocialGraph {
	private SimpleWeightedGraph<Developer, DefaultWeightedEdge> socialGraph;
	final private int EDGE_WEIGHT_CAP = 10000;
	
	/**
	 * The constructor
	 */
	public SocialGraph() {
		socialGraph = new SimpleWeightedGraph<Developer, DefaultWeightedEdge>(DefaultWeightedEdge.class);
	}
	
	/**
	 * Add a new developer/vertex to the social graph
	 * @param d
	 * 		The new developer
	 */
	public void addUser(Developer d) {
		if (!socialGraph.containsVertex(d)) {
			socialGraph.addVertex(d);
		}
	}
	
	/**
	 * Adds an edge between two developers if it doesn't already exist. If the
	 * edge already exists, the weight is updated.
	 * @param d1
	 * @param d2
	 * @param weight
	 */
	public void addConnection(Developer d1, Developer d2, double weight) {
		if (socialGraph.getEdge(d1, d2) == null) {
			//Add the new edge
			socialGraph.addEdge(d1, d2, new DefaultWeightedEdge());
			socialGraph.setEdgeWeight(socialGraph.getEdge(d1, d2), weight);
		} else {
			//Update the weight
			DefaultWeightedEdge edge = socialGraph.getEdge(d1, d2);
			double currentEdgeWeight = socialGraph.getEdgeWeight(edge);
			socialGraph.setEdgeWeight(edge, currentEdgeWeight+weight);
		}
	}
	
	/**
	 * Retrieve the social graph
	 * @return
	 */
	public SimpleWeightedGraph<Developer, DefaultWeightedEdge> getSocialGraph() {
		return socialGraph;
	}
	
	/**
	 * Adjusts the edge weights x using function f(x), which is defined as
	 * follows (where n is the total number of users/vertices):
	 * 
	 * f(x) = (1/x)n, for x > 0
	 * f(x) = y, for x = 0 and y sufficiently large
	 * 
	 * In this case, since (1/x)n has a maximum possible value of n (i.e., when
	 * x = 1), then y must be set to some value > n. The value for
	 * y is representedby EDGE_WEIGHT_CAP.
	 */
	public void adjustAllEdges() {
		Set<Developer> developerSet = socialGraph.vertexSet();
		Iterator<Developer> developerSetIter = developerSet.iterator();
		while (developerSetIter.hasNext()) {
			Developer d1 = developerSetIter.next();
			Iterator<Developer> newDeveloperSetIter = developerSet.iterator();
			while (newDeveloperSetIter.hasNext()) {
				Developer d2 = newDeveloperSetIter.next();
				if (d2.getEmail().equals(d1.getEmail())) {
					continue; //no loops
				}
				DefaultWeightedEdge edge = socialGraph.getEdge(d1, d2);
				if (edge == null) {
					addConnection(d1, d2, EDGE_WEIGHT_CAP);
				}
				else {
					double adjustedWeight = (1.0 / (socialGraph.getEdgeWeight(edge)*1.0)) * developerSet.size();
					socialGraph.setEdgeWeight(edge, adjustedWeight);
				}
			}
		}
	}
}