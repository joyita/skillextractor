package com.project.cpsc534l.social;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleWeightedGraph;

import com.project.cpsc534l.skills.extractor.BugReport;
import com.project.cpsc534l.skills.extractor.Developer;

public class SocialGraphMaker {
	//private String pathToTempFolder;
	//private int maxNumBugReports;
	//private List<BugReport> bugReports;
	private Map<String, Developer> developers;
	
	private SocialGraph socialGraph;
	
	/**
	 * Constructor
	 */
	public SocialGraphMaker(Map<String, Developer> _developers) {
		//pathToTempFolder = _pathToTempFolder;
		//maxNumBugReports = _maxNumBugReports;
		//bugReports = _bugReports;
		developers = _developers;
		
		socialGraph = new SocialGraph();
	}
	
	/**
	 * Create the social graph based on which bug reports developers
	 * have "collaborated" on (where collaboration is defined as
	 * two developers commenting on the same bug report)
	 */
	public void makeSocialGraph() {
		System.out.println("Making social graph...");
		Set<String> firstDeveloperEmails = developers.keySet();
		for (String firstDeveloperEmail : firstDeveloperEmails) {
			Developer firstDeveloper = developers.get(firstDeveloperEmail);
			socialGraph.addUser(firstDeveloper);
			List<BugReport> firstBugReports = firstDeveloper.getBugReports();
			Set<String> secondDeveloperEmails = developers.keySet();
			for (String secondDeveloperEmail : secondDeveloperEmails) {
				Developer secondDeveloper = developers.get(secondDeveloperEmail);
				if (secondDeveloperEmail.equals(firstDeveloperEmail)) {
					//Ignore
					continue;
				}
				socialGraph.addUser(secondDeveloper);
				
				//Find the bug reports where both developers have commented
				List<BugReport> secondBugReports = secondDeveloper.getBugReports();
				int numCollaborations = 0;
				for (int i = 0; i < firstBugReports.size(); i++) {
					BugReport nextBr = firstBugReports.get(i);
					if (secondBugReports.contains(nextBr)) { //Note that equals is overriden for BugReport
						//Matching BRs
						numCollaborations++;
					}
				}
				socialGraph.addConnection(firstDeveloper, secondDeveloper, numCollaborations);
			}
		}
		
		socialGraph.adjustAllEdges();
	}
	
	/**
	 * Returns the social graph
	 * @return
	 */
	public SimpleWeightedGraph<Developer, DefaultWeightedEdge> getSocialGraph() {
		return socialGraph.getSocialGraph();
	}
}